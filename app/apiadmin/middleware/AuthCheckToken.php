<?php
/**
 * 重庆柯一网络有限公司
 * 遵循MT协议，开源并可商业使用，没有任何限制
 * @Author:cqkyi
 * @Date: 2020/10/2 16:11
 * 官方惟一地址：www.cqkyi.com
 */

namespace app\apiadmin\middleware;


use app\common\service\JwtService;
use app\common\utils\ErrorCode;
use app\common\utils\Massage;
use app\common\utils\Res;

class AuthCheckToken
{
    public function handle($request, \Closure $next){
        $token = trim(ltrim($request->header('Authorization'), 'Bearer'));
        if(empty($token)){
            return Res::error(Massage::ROULE);
        }
        $result = JwtService::checkToken($token);
        if($result['code']!=ErrorCode::SUCCESS){
            return Res::jsonResult($result);
        }
        $request->authInfo=$result;
        $request->uid=$result['data']->uid;
        $request->userName=$result['data']->user_name;
        $request->face = $result['data']->face;
        return $next($request);
    }
}