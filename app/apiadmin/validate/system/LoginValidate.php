<?php


namespace app\apiadmin\validate\system;


use think\Validate;

class LoginValidate extends Validate
{

    protected $rule = [
        'user_name'  =>  'require|unique:sys_member',
        'password' =>  'require',
        'code'=>'require'
    ];
    protected $message = [
        'user_name.require'=>'管理员账号必填~~',
        'password.require'=>'密码必填~~',
        'code.require'=>'验证码不能为空'
    ];

    public function sceneApi()
    {
        return $this->only(['user_name','password','code'])
            ->remove('user_name', 'unique');
    }

}