<?php


namespace app\apiadmin\controller\system;


use app\apiadmin\service\system\MemberService;
use app\common\controller\BaseController;

class Member extends BaseController
{

    public function info(){
        $uid = $this->request->uid;
        $userName = $this->request->userName;
        $face = $this->request->face;
        $result = MemberService::memberInfo($uid,$userName,$face);
        return $result;
    }

    public function index(){
        $param = $this->request->post();
        $result = MemberService::list($param);
        return $result;
    }


    public function add(){
        $param = $this->request->post();
        $param['creat_time']=date('Y-m-d H:i:s',time());
        $param['creat_by']=$this->request->userName;
        $param['password']=md5($param['password']);
        $result = MemberService::add($param);
        return $result;
    }

    public function edit(){
        $param = $this->request->post();
        $param['update_by']=$this->request->userName;
        $param['update_time']=date('Y-m-d H:i:s',time());
        $result = MemberService::edit($param);
        return $result;
    }

    public function del($id){
        $result = MemberService::del($id);
        return $result;
    }

}