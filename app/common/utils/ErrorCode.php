<?php
/**
 * 重庆柯一网络有限公司
 * 遵循MT协议，开源并可商业使用，没有任何限制
 * @Author:cqkyi
 * @Date: 2020/10/2 16:14
 * 官方惟一地址：www.cqkyi.com
 */

namespace app\common\utils;


class ErrorCode
{
    const SUCCESS =200;
    const ERROR = 201;
    const TOKENERROR = 401;


}